const validateLanguages = ['html', 'css', 'javascript', 'php', 'sql', 'java', 'python', 'c++'];

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('dev', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name:  {
            type: DataTypes.STRING,
            allowNull: false,
            unique: {
                msg: 'Le nom doit être unique.'
            },
            validate : {
                notNull: {msg : "Le nom est obligatoire"},
                min : {args : [3], msg : "Le nom doit faire au moins 3 caractères"},
            }    
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate : {
                isInt: {msg : "L'âge doit être un nombre entier"},
                notNull: {msg : "L'âge est obligatoire"},
            }
        },
        language: {
            type: DataTypes.STRING,
            allowNull: false,
            get() {
                // return this.getDataValue('language').split(',');
                const language = this.getDataValue('language');
                if (language) {
                  return language.split(',');
                } else {
                  return [];
                }
            },
            set(language) {
                // this.setDataValue('language', language.join());
                if (Array.isArray(language)) {
                    this.setDataValue('language', language.join());
                } else {
                    this.setDataValue('language', language);
                }
            },
            validate: {
                isLangageValid(value) { // Fix the parameter name to lowercase "value"
                  if (!value) {
                    throw new Error('Vous devez choisir au moins un langage');
                  }
                  if (value.split(',').length > 3) {
                    throw new Error('Vous ne pouvez pas choisir plus de 3 langages');
                  }
                  value.split(',').forEach((langage) => {
                    if (!validateLanguages.includes(langage)) {
                      throw new Error('Le langage ' + langage + " n'est pas valide");
                    }
                  });
                },
              },
        },
        niveau: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate : {
                isInt: {msg : "Le niveau doit être un nombre entier"},
                notNull: {msg : "Le niveau est obligatoire"},
                min : {args : [0], msg : "Zero est le niveau minimum"},
                max : {args : [999], msg : "999 est le niveau maximum"},
            }    
        },
        picture: {
            type: DataTypes.STRING,
            allowNull: true,
            validate : {
                isUrl: {msg : "L'url de l'image n'est pas valide"},
            }
        }    
    }, {
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: false,
    });
}

