const { Op } = require('sequelize');
const { Dev } = require('../db/sequelize');
const auth = require('../auth/auth');

module.exports = (app) => {
    // added auth middleware on this route
    app.get('/api/devs', auth, (req, res) => {
        if (req.query.name) {
        const name = req.query.name;
        const limit = parseInt(req.query.limit) || 5;
        if(name.length < 4) {
            const message = 'Le nom de la recherche doit contenir au moins 4 caractères.';
            return res.status(400).json({ message });
        }
        Dev.findAndCountAll({
            where: {
                name: {
                    [Op.like]: `%${name}%`,
                },
            },
            order: [['name', 'ASC']],
            limit: limit
            })
            .then (({ count, rows }) => {
                const message = `Il y a ${count} développeur(s) avec le nom ${name}.`;
                res.json({ message, data: rows });
            })
        } else {
            Dev.findAll( { order: [['name', 'ASC']] } )
            .then((devs) => {
                const message = 'Tous les développeurs ont été trouvés !';
                res.json({ message, data: devs });
            })
            .catch((error) => {
                const message = 'La liste des développeurs n\'a pas pu être récupérée. Réessayez dans quelques instants.';
                res.status(500).json({ message, data: error });
            });
        }
    })
}                 





//         .then(({ count, rows }) => {
//           if (rows.length === 0) {
//             const message = `Aucun développeur trouvé avec le nom ${name}`;
//             res.json({ message, data: rows });
//           } else {
//             const message = `Il y a ${count} développeur(s) avec le nom ${name}.`;
//             res.status(200).json({ message, data: rows });
//           }
//         })
//         .catch((error) => {
//           const message = `Le développeur avec le nom ${name} n'a pas pu être récupéré. Réessayez dans quelques instants.`;
//           res.status(500).json({ message, data: error });
//         });
//     } else {
//       Dev.findAll()
//         .then((devs) => {
//           const message = 'Tous les développeurs ont été trouvés !';
//           res.json({ message, data: devs });
//         })
//         .catch((error) => {
//           const message = 'La liste des développeurs n\'a pas pu être récupérée. Réessayez dans quelques instants.';
//           res.status(500).json({ message, data: error });
//         });
//     }
//   });
// };

