const { Dev } = require('../db/sequelize');
const { ValidationError, UniqueConstraintError } = require('sequelize');
const auth = require('../auth/auth');

module.exports = (app) => {
    app.put('/api/devs/:id', auth, (req, res) => {
        const id = req.params.id;
        Dev.update(req.body, {
            where: { id: id }
        })
            .then(dev => {
                // return permet de factoriser les deux cas d'erreur suivants en un seul
                return Dev.findByPk(id).then(dev => {
                    if (dev === null) {
                        const message = `Le développeur avec l'id ${id} n'a pas été trouvé. Réessayez avec un autre identifiant.`;
                        return res.status(404).json({ message });
                    }
                    const message = `Le développeur avec l'id ${id} a été modifié avec succès !`;
                    res.json({ message, data: dev });
                })
            })
            .catch(error => {
                if(error instanceof ValidationError) {
                    return res.status(400).json({ message: error.message, data: error });
                }
                if(error.name === 'UniqueConstraintError') {
                    return res.status(400).json({ message: error.message, data: error });
                }
                const message = `Le développeur avec l'id ${id} n'a pas pu être modifié. Réessayez dans quelques instants.`;
                res.status(500).json({ message, data: error });
            });
    })
}