const { Dev } = require('../db/sequelize');
const auth = require('../auth/auth');

module.exports = (app) => {
    app.delete('/api/devs/:id', auth, (req, res) => {
        const dev = Dev.findByPk(req.params.id)
            .then(dev => {
                if (dev === null) {
                    const message = `Le développeur avec l'id ${req.params.id} n'a pas été trouvé. Réessayez avec un autre identifiant.`;
                    return res.status(404).json({ message });
                }

                const devToDelete = dev;
                return Dev.destroy({
                    where: { id: dev.id }
                })
                .then(_ => {
                    const message = `Le développeur ${devToDelete.name} a bien été supprimé !`;
                    res.json({ message, data: devToDelete });
                })
            })
            .catch(error => {
                const message = `Le développeur avec l'id ${req.params.id} n'a pas pu être récupéré. Réessayez dans quelques instants.`;
                res.status(500).json({ message, data: error });
            });
    })
}