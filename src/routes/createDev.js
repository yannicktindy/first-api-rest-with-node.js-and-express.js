const { Dev } = require('../db/sequelize');
const { ValidationError, UniqueConstraintError } = require('sequelize');
const auth = require('../auth/auth');

module.exports = (app) => {
    app.post('/api/devs', auth, (req, res) => {
        Dev.create(req.body)
        .then(dev => {
            const message = `Le développeur ${dev.name} a été créé avec succès !`;
            res.json({ message, data: dev });
        })
        .catch (error => {
            if(error instanceof ValidationError) {
                return res.status(400).json({ message: error.message, data: error });
            }
            if(error.name === 'UniqueConstraintError') {
                return res.status(400).json({ message: error.message, data: error });
            }
            const message = 'Le développeur n\'a pas pu être créé. Réessayez dans quelques instants.';
            res.status(500).json({ message, data: error });     
        });
    })
}