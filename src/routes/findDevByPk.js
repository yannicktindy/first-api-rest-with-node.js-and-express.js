const { Dev } = require('../db/sequelize');
const auth = require('../auth/auth');

module.exports = (app) => {
    app.get('/api/devs/:id', auth, (req, res) => {
        Dev.findByPk(req.params.id)
            .then(dev => {
                if (dev === null) {
                    const message = `Le développeur avec l'id ${req.params.id} n'a pas été trouvé. Réessayez avec un autre identifiant.`;
                    return res.status(404).json({ message });
                }
                const message = `Le développeur avec l'id ${req.params.id} a été trouvé !`;
                res.json({ message, data: dev });
            })
            .catch(error => {   
                const message = `Le développeur avec l'id ${req.params.id} n'a pas pu être récupéré. Réessayez dans quelques instants.`;
                res.status(500).json({ message, data: error });
            });
    })
}        