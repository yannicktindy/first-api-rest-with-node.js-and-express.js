// this is the middleware for authentication
const jwt = require('jsonwebtoken');
const privateKey = require('./private_key');

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        const message = 'Vous n\'avez pas fourni de jeton d\'authentification. Ajoutez-en un dans le header de votre requête.';
        return res.status(401).json({ message });
    }
    
    const token = authHeader.split(' ')[1];
    let decodedToken;
    try {
        decodedToken = jwt.verify(token, privateKey);
        req.userData = { userId: decodedToken.userId };
        next();
    } catch (error) {
        const message = 'L\'utilisateur n\'est pas autorisé à accéder à cette ressource.';
        return res.status(401).json({ message });
    }
};
