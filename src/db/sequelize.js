const { Sequelize, DataTypes } = require('sequelize');
const DevModel = require('../models/dev');
const UserModel = require('../models/user');
const devs = require('./mock-devs');
const bcrypt = require('bcrypt');

const sequelize = new Sequelize('node_devs', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    dialectOptions: {
        timezone: '+02:00',
    },
    logging: (message) => {
        console.log(`Sequelize log: ${message}`);
    },
});

const Dev = DevModel(sequelize, DataTypes);
const User = UserModel(sequelize, DataTypes);

const initDb = () => {
    sequelize.sync({ force: true}) // supprimer force: true pour le depoiement
        .then(() => {
            console.log('Database tables created and synchronized successfully !');

            devs.map(dev => {
                Dev.create({
                    name: dev.name,
                    age: dev.age,
                    language: dev.language,
                    niveau: dev.niveau,
                    picture: dev.picture,
                }) .then(dev => console.log(dev.toJSON()));
            });

            bcrypt.hash("mick", 10)
            .then(hash =>User.create({ username: "mick", password: hash }))
            .then(user => console.log(user.toJSON()));
    }) 
}  

module.exports = {
    initDb, Dev, User
}    