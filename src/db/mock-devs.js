const devs= [
    {
        "id": 1,
        "name": "John",
        "age": 30,
        "language": ["javascript", "html", "css"],
        "niveau": 1,
        "picture": "dev1.png"
    },
    {
        "id": 2,
        "name": "James",
        "age": 25,
        "language": ["php", "java"],
        "niveau": 1,
        "picture": "dev2.png"
    },
    {
        "id": 3,
        "name": "Jack",
        "age": 20,
        "language": ["python", "sql"],
        "niveau": 1,
        "picture": "dev3.png"
    },
    {
        "id": 4,
        "name": "Sam",
        "age": 15,
        "language": ["java", "c++"],
        "niveau": 1,
        "picture": "dev4.png"
    },
    { 
        "id": 5,
        "name": "Nick",
        "age": 10,    
        "language": ["html", "css"],
        "niveau": 1,
        "picture": "dev5.png"
    },
    {
        "id": 6,
        "name": "koddy",
        "age": 5,
        "language": ["sql"],
        "niveau": 1,
        "picture": "dev6.png"
    }
];

module.exports = devs;