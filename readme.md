installer express
installer nodemon : npm install nodemon --save-dev
installer morgan middleware de log  : npm install morgan --save-dev
installer in favicon : npm install serve-favicon --save
insomnia pour tester le post
installer le body parser pour le middleware pour parser les datas entrante dans l'api : npm install body-parser --save
installation ORM sequelize : npm install sequelize --save
installer le driver mysql : 
npm install mysql --save
npm install mysql2 --save
npm install bcrypt --save
npm install jsonwebtoken --save

create heroku account
install heroku CLI
in terminal : heroku login
heroku update

----------------------------------
CONSIGNE AVANT LA MISE EN PROD !!!
----------------------------------

ne pas utiliser nodemon en prod & passer express en prod :
- modifier les scripts dans le package.json
    "start": "NODE.ENV=production node app.js",
    "dev": "NODE.ENV=developement nodemon app.js" 


ne pas utiliser les dépendances de dev en production
- supprimer "morgan": "^1.10.0", des dev dependencies (middleware morgan = log de status)
- supprimer les importations de morgan dans app.js

----------------------------------
CONSIGNE HEROKU CLI !!!
----------------------------------
Le projet doit etre initialisé avec git
git add .
git commit -m "commit before prod"

vscode ne reconnaissait pas les commandes heroku :
- instalation du plugins heroku cli
- redemarrage de vs code

commande : 
heroku login
heroku create

git push heroku master

------------------------------------------
Heroku is not free for students anymore :( 

NO DEPLOY !!!!!!!!!!!!!!!!!!!!!!!!!!!!
------------------------------------------

npm install cors --save
le paquet cors s'utilise comme un middleware



https://snyk.io/blog/10-best-practices-to-containerize-nodejs-web-applications-with-docker/