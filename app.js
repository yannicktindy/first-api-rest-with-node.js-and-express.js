const express = require('express');
// const morgan = require('morgan');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const sequelize = require('./src/db/sequelize');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 300

app
    .use(bodyParser.json())
    // .use(express.json())
    .use(favicon(__dirname + '/favicon.ico'))
    .use(cors())
    // .use(morgan('dev'))

app.post('/', (req, res) => {
    res.send('Hello Heroku!')
})

sequelize.initDb();    

// ici futur pooints de terminaison de l'API
require('./src/routes/findAllDevs')(app);
require('./src/routes/findDevByPk')(app);
require('./src/routes/createDev')(app);
require('./src/routes/updateDev')(app);
require('./src/routes/deleteDev')(app);
require('./src/routes/login')(app);

app.use(({res}) => {
    const message = 'Impossible de trouver la ressource demandée ! Vpus devez essayer une autre URL.';
    res.status(404).json({ message });
});


app.listen(port,()=> console.log(`Server is running on port http://localhost${port}`));    




