exports.success = (message, data) => {
    return { message, data };
}

exports.getUniqueId = (dev) => {
    if (dev.length === 0) {
      return 1; // Return an initial ID when the array is empty
    }
  
    const devIds = dev.map((dev) => dev.id);
    const maxID = Math.max(...devIds);
    const uniqueId = maxID + 1;
  
    return uniqueId;
  };

// exports.getUniqueId = (dev) => {
//     const devIds =  dev.map(dev => dev.id);
//     const maxID = Math.max(...devIds);
//     const uniqueId = maxID + 1;

//     return uniqueId;
// }